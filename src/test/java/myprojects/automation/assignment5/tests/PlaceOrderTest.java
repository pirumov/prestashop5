package myprojects.automation.assignment5.tests;

import myprojects.automation.assignment5.BaseTest;
import myprojects.automation.assignment5.model.ProductData;
import myprojects.automation.assignment5.utils.DataConverter;
import myprojects.automation.assignment5.utils.Properties;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class PlaceOrderTest extends BaseTest {
    ProductData inShop, inCart;

    @Test
    @Parameters({"isMobile"})
    public void checkSiteVersion(String isMobile) {
        // TODO open main page and validate website version
        actions.openShop();
        if (driver.findElement(By.id("menu-icon")).isDisplayed() && !isMobile.equals("")){
            Reporter.log("Mobiled ver. opened!");
            System.out.println("Mobile ver. opened!");
        } else {
            Reporter.log("Desktop ver. opened!");
            System.out.println("Desktop ver. opened!");
        }

    }

    @Test
    public void createNewOrder() {
        // TODO implement order creation test

        // open random product
        actions.openRandomProduct();

        // save product parameters
        inShop = actions.getOpenedProductInfo();

        // add product to Cart and validate product information in the Cart
        actions.addToCart();

        Assert.assertEquals(1, driver.findElements(By.xpath("//*[@id=\"main\"]/div/div[1]/div[1]/div[2]/ul")).size());

        String name = driver.findElements(By.className("product-line-info")).get(0).getText().toUpperCase();
        String price = driver.findElements(By.className("product-line-info")).get(1).getText();

        Assert.assertEquals(inShop.getName(), name, "Name isn't equal");
        Assert.assertEquals(inShop.getPrice(), DataConverter.parsePriceValue(price), "Price isn't equal");

        driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/div/div[2]/div/a")).click();

        // proceed to order creation, fill required information

        // Name
        driver.findElement(By.xpath("//input[@name='firstname']")).sendKeys("Abdurakhman");
        driver.findElement(By.xpath("//input[@name='lastname']")).sendKeys("Abdurakhmanov");
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("abdurakhmanov@gmail.com");
        driver.findElement(By.xpath("//button[@name='continue']")).click();

        // Address
        driver.findElement(By.xpath("//input[@name='address1']")).sendKeys("Porika st.");
        driver.findElement(By.xpath("//input[@name='postcode']")).sendKeys("01001");
        driver.findElement(By.xpath("//input[@name='city']")).sendKeys("Ashkhabad");
        driver.findElement(By.xpath("//button[@name='confirm-addresses']")).click();

        // Delivery
        driver.findElement(By.xpath("//button[@name='confirmDeliveryOption']")).click();

        // Payment
        driver.findElement(By.id("payment-option-1")).click();
        driver.findElement(By.id("conditions_to_approve[terms-and-conditions]")).click();
        driver.findElement(By.xpath("//*[@id=\"payment-confirmation\"]/div[1]/button")).click();

        // place new order and validate order summary
        Assert.assertTrue(driver.findElement(By.id("content-hook_order_confirmation")).findElement(By.tagName("h3"))
                .getText().contains("ВАШ ЗАКАЗ ПОДТВЕРЖДЁН"), "Wrong confirmation message!");

        Assert.assertTrue(driver.findElement(By.id("order-items"))
                .findElements(By.cssSelector(".order-line .row")).size() == 1, "More than 1 position");

        String pricePurchased = driver.findElement(By.id("order-items"))
                .findElement(By.cssSelector(".order-line .row")).findElements(By.tagName("div")).get(0).getText();
        Assert.assertEquals(DataConverter.parsePriceValue(pricePurchased), inShop.getPrice(), "Price isn't equal");

        String namePurchased = driver.findElement(By.xpath("//*[@id=\"order-items\"]/div/div/div[2]/span")).getText().toUpperCase();
        Assert.assertTrue(namePurchased.contains(inShop.getName().toUpperCase()), "Wrong Name!");

        // check updated In Stock value
        driver.get(Properties.getBaseUrl());
        actions.openPurchasedProduct();
        Assert.assertEquals(actions.getQty(),inShop.getQty()-1, "Qty did not decrease after the order");
    }

}
