package myprojects.automation.assignment5.utils;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class DriverFactory {
    /**
     * @param browser Driver type to use in tests.
     * @return New instance of {@link WebDriver} object.
     */
    public static WebDriver initDriver(String browser, String isMobile) {
        String os = Properties.getOS();

        //OS check only for use drivers for Mac or Windows
        if (!os.equals("Mac OS X")) {
            switch (browser) {
                case "firefox":
                    System.setProperty(
                            "webdriver.gecko.driver",
                            new File(DriverFactory.class.getResource("/geckodriver.exe").getFile()).getPath());
                    return new FirefoxDriver();
                case "ie":
                case "internet explorer":
                    System.setProperty(
                            "webdriver.ie.driver",
                            new File(DriverFactory.class.getResource("/IEDriverServer.exe").getFile()).getPath());
                    DesiredCapabilities capabilitiesIE = DesiredCapabilities.internetExplorer();
                    capabilitiesIE.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
                    capabilitiesIE.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
                    return new InternetExplorerDriver(capabilitiesIE);
                case "phantomjs":
                    System.setProperty(
                            "phantomjs.binary.path",
                            new File(DriverFactory.class.getResource("/phantomjs.exe").getFile()).getPath());
                    return new PhantomJSDriver();
                case "chrome":
                default:
                    System.setProperty(
                            "webdriver.chrome.driver",
                            new File(DriverFactory.class.getResource("/chromedriver.exe").getFile()).getPath());
                    return new ChromeDriver();
            }
        } else {
            switch (browser) {
                case "firefox":
                    System.setProperty(
                            "webdriver.gecko.driver",
                            new File(DriverFactory.class.getResource("/geckodriver").getFile()).getPath());
                    return new FirefoxDriver();
                case "chrome":
                default:
                    System.setProperty(
                            "webdriver.chrome.driver",
                            new File(DriverFactory.class.getResource("/chromedriver").getFile()).getPath());
                    if (isMobile.equals("")){
                        return new ChromeDriver();
                    } else {
                        Map<String, String> mobileEmulation = new HashMap<String, String>();
                        mobileEmulation.put("deviceName", "Nexus 5X");
                        Map<String, Object> chromeOptions = new HashMap<String, Object>();
                        chromeOptions.put("mobileEmulation", mobileEmulation);
                        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
                        return new ChromeDriver(capabilities);
                    }
            }
        }
    }

    /**
     * @param browser Remote driver type to use in tests.
     * @param gridUrl URL to Grid.
     * @return New instance of {@link RemoteWebDriver} object.
     */
    public static WebDriver initDriver(String browser, String isMobile, String gridUrl) throws MalformedURLException {
        // TODO prepare capabilities for required browser and return RemoteWebDriver instance
        DesiredCapabilities capabilities;
        switch (browser) {
            case "firefox":
                capabilities = DesiredCapabilities.firefox();
                break;
            case "ie":
            case "internet explorer":
                capabilities = DesiredCapabilities.internetExplorer();
                break;
            case "phantomjs":
                capabilities = DesiredCapabilities.phantomjs();
                break;
            case "chrome":
            default:
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverFactory.class.getResource("/chromedriver").getFile()).getPath());
                if (isMobile.equals("")) {
                    capabilities = DesiredCapabilities.chrome();
                    capabilities.setPlatform(Platform.MAC);
                } else {
                    Map<String, String> mobileEmulation = new HashMap<String, String>();
                    mobileEmulation.put("deviceName", "Nexus 5X");
                    Map<String, Object> chromeOptions = new HashMap<String, Object>();
                    chromeOptions.put("mobileEmulation", mobileEmulation);
                    capabilities = DesiredCapabilities.chrome();
                    capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
                }
                break;
        }

        return new RemoteWebDriver(new URL(gridUrl), capabilities);
    }
}
