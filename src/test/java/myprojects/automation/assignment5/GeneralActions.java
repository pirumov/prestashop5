package myprojects.automation.assignment5;


import myprojects.automation.assignment5.model.ProductData;
import myprojects.automation.assignment5.utils.DataConverter;
import myprojects.automation.assignment5.utils.Properties;
import myprojects.automation.assignment5.utils.logging.CustomReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;

    public static int productIndex;

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void openRandomProduct() {
        // TODO implement logic to open random product before purchase

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"content\"]/section/a")));
         // click on "Vse tovary :)"
        driver.findElement(By.xpath("//*[@id=\"content\"]/section/a")).click();

        List<WebElement> articles = driver.findElements(By.tagName("article")); // find all products

        Random random = new Random();
        productIndex = random.nextInt(articles.size()-1);

        // find all products and click random
        articles.get(productIndex).findElement(By.cssSelector(".product-description"))
                .findElement(By.tagName("a")).click();
    }

    public void openPurchasedProduct() {
        // TODO implement logic to open random product before purchase
        driver.findElement(By.xpath("//*[@id=\"content\"]/section/a")).click(); // click on "Vse tovary :)"

        List<WebElement> articles = driver.findElements(By.tagName("article")); // find all products

        // find all products and click random
        articles.get(productIndex).findElement(By.cssSelector(".product-description"))
                .findElement(By.tagName("a")).click();
    }


    public void addToCart () {
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[2]/div[1]/div[2]/button"))));
        driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[2]/div[1]/div[2]/button")).click();

        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("myModalLabel"))));
        driver.findElement(By.xpath("//*[@id=\"blockcart-modal\"]/div/div/div[2]/div/div[2]/div/a")).click();
    }

    public void openShop () {
        driver.get(Properties.getBaseUrl()); // load prestashop home page
    }


    /**
     * Extracts product information from opened product details page.
     *
     * @return
     */
    public ProductData getOpenedProductInfo() {
        CustomReporter.logAction("Get information about currently opened product");
        // TODO extract data from opened page

        // get Name
        String name = driver.findElement(By.xpath("//h1[@itemprop='name']")).getText();

        // get price from site
        float price = DataConverter.parsePriceValue(driver.findElement(By.xpath("//span[@itemprop='price']")).getText());
        // get qty from site

        return new ProductData(name, getQty(), price);
    }

    public int getQty() {
        CustomReporter.logAction("Get product qty");
        int qty;

        // get qty from site
        driver.findElement(By.xpath("//*[@id=\"main\"]/div[1]/div[2]/div[2]/div[3]/ul/li[2]/a")).click();

        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("product-details"))
                .findElement(By.className("product-quantities"))
                .findElement(By.tagName("span"))));
        qty = DataConverter.parseStockValue(driver.findElement(By.id("product-details"))
                .findElement(By.className("product-quantities"))
                .findElement(By.tagName("span")).getText());
        return qty;
    }
}
